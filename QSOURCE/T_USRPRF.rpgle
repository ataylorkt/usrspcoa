**FREE
Ctl-Opt ActGrp(*New) BndDir('USRSPCOA');

// The file layout used for the F spec should match the "list item" data structure
Dcl-F OBJL0100_T Disk(*Ext) UsrOpn Handler('USRSPCOA(USRSPC_Handler)':UserSpaceDS);

/COPY QSOURCE,COMMON_H

Dcl-Ds UserSpaceDS LikeDS(QualObjNameDS_t) Inz(*LikeDS);

Dcl-Pi *N;
  pObjectName Char(10);
  pLibraryName Char(10);
End-Pi;

// Specify the qualified name of the user space being accessed
reset UserSpaceDS;

if pObjectName = *Blanks;
  dsply 'ERROR: Need object name';
  return;
else;
  UserSpaceDS.ObjectName = pObjectName;
endif;

if pLibraryName = *Blanks;
  dsply 'ERROR: Need library name';
  return;
else;
  UserSpaceDS.LibraryName = pLibraryName;
endif;

// This simply creates and populates a user space with data - specifically a
//   list of all *USRPRF objects in QSYS.
SetUpUserSpace( UserSpaceDS );

// Read through the user space and output all user profiles that were found
open OBJL0100_T;

read OBJL0100_T;
dow not %EOF(OBJL0100_T);
  dsply OBJNAME;
  read OBJL0100_T;
enddo;

close OBJL0100_T;

return;

Dcl-Proc SetUpUserSpace;
  Dcl-Pi *N;
    pQualDS LikeDS(QualObjNameDS_t) Const;
  End-Pi;

  Dcl-Pr ListObjects Extpgm('QUSLOBJ');
    QualUserSpace LikeDS(QualObjNameDS_t) Const;
    FormatName Char(8) Const;
    QualSearchObject LikeDS(QualObjNameDS_t) Const;
    QualSearchObjectType Char(10) Const;
    QUSECDS LikeDS(QUSECDS_t) Options(*Varsize:*Nopass);
  End-Pr;

  Dcl-Ds SearchObjDS LikeDS(QualObjNameDS_t) Inz(*LikeDS);
  Dcl-S SearchType Char(10);

  Dcl-Ds ErrDS LikeDS(QUSECDS_t) Inz(*LikeDS);

  /COPY QSOURCE,SPCA0100
  /COPY QSOURCE,USRSPC_H

  // Initialize our QUSEC error data structure and set the BytesProvided to 8
  //  This forces any API we give it to to throw an exception instead of
  //  simply returning the data in ErrDS for us to silently ignore.
  reset ErrDS;
  ErrDS.BytesProvided = 8;

  // Starting by creating a user space
  CreateUserSpace( pQualDS
                 : 'TEST'
                 : 1024
                 : USRSPC_INZ_NULL
                 : USRSPC_PUB_AUT_ALL
                 : ''
                 : USRSPC_REPLACE_YES
                 : ErrDS
                 : USRSPC_DOMAIN_SYSTEM
                 : USRSPC_DFT_TRANSFER_SIZE
                 : USRSPC_ALIGN_OPTIMUM );

  reset SearchObjDS;
  SearchObjDS.LibraryName = 'QSYS';
  SearchObjDS.ObjectName = '*ALL';
  SearchType = '*USRPRF';

  reset ErrDS;
  ErrDS.BytesProvided = 8;
  ListObjects( pQualDS
             : 'OBJL0100'
             : SearchObjDS
             : SearchType
             : ErrDS );



End-Proc;
