**FREE
Ctl-Opt NoMain;

/COPY "COMMON_H.rpgleinc"
/COPY "USRSPC_H.rpgleinc"
/COPY "SPCA0100.rpgleinc"

/COPY QOAR/QRPGLESRC,QRNOPENACC

// Generic header data structures for processing user spaces containing lists
// Ref: https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_72/apiref/usfcommonstructure.htm
Dcl-C GENHDR_SIZE %Size(GENHDR_t);
Dcl-Ds GENHDR_t Qualified Template Inz;
  UserArea Char(64);
  SizeOfHeader Int(10);
End-Ds;

// This is the generic header format for list APIs called as programs
Dcl-C GENHDR0100_SIZE %Size(GENHDR0100_t);
Dcl-Ds GENHDR0100_t Qualified Template Inz;
  UserArea Char(64);
  SizeOfHeader Int(10);
  StructLevel Char(4);
  FormatName Char(8);
  APIUsed Char(10);
  DateTimeCreated Char(13);
  InfoStatus Char(1);
  UserSpaceSize Int(10);
  InputOffset Int(10);
  InputSize Int(10);
  HeaderOffset Int(10);
  HeaderSize Int(10);
  ListOffset Int(10);
  ListSize Int(10);
  NumofEntries Int(10);
  EntrySize Int(10);
  EntryCCSID Int(10);
  CountryRegionID Char(2);
  LanguageID Char(3);
  SubsetListInd Char(1);
  *N Char(42);
End-Ds;

// This is the generic header format for list APIs called as subprocedures
Dcl-C GENHDR0300_SIZE %Size(GENHDR0300_t);
Dcl-Ds GENHDR0300_t Qualified Template Inz;
  UserArea Char(64);
  SizeOfHeader Int(10);
  StructLevel Char(4);
  FormatName Char(8);
  APIUsed Char(10);
  DateTimeCreated Char(13);
  InfoStatus Char(1);
  UserSpaceSize Int(10);
  InputOffset Int(10);
  InputSize Int(10);
  HeaderOffset Int(10);
  HeaderSize Int(10);
  ListOffset Int(10);
  ListSize Int(10);
  NumOfEntries Int(10);
  EntrySize Int(10);
  EntryCCSID Int(10);
  CountryRegionID Char(2);
  LanguageID Char(3);
  SubsetListInd Char(1);
  *N Char(42);
  APIEntryPointName Char(256);
  *N Char(128);
End-Ds;

// RPG Open Access handler subprocedure which can process a user space filled with
//  data from a "List" API using native RPG IO.

Dcl-Proc USRSPC_Handler Export;
  Dcl-Pi *N ExtProc('USRSPC_Handler');
    pOAInfoDS LikeDS(QrnOpenAccess_T);
  End-Pi;

  Dcl-Ds QualDS LikeDS(QualObjNameDS_t) Based(pOAInfoDS.userArea);

  Dcl-Ds StateDS Qualified Based(pOAInfoDS.stateInfo);
    UserSpaceSize Int(10);
    ListOffset Int(10);
    RecordFormat Char(8);
    RecordCount Int(10);
    CurrentRecord Int(10);
  End-Ds;

  Dcl-S Buffer Char(256000) Based(pOAInfoDS.inputBuffer);
  Dcl-Ds ErrDS LikeDS(QUSECDS_t) Inz(*LikeDS);

  Dcl-Ds GENHDR LikeDS(GENHDR_t);
  Dcl-Ds GENHDR0100 LikeDS(GENHDR0100_t);
  Dcl-Ds GENHDR0300 LikeDS(GENHDR0300_t);
  Dcl-Ds SPCADS LikeDS(SPCA0100_t) Inz(*LikeDS);

  Dcl-S Offset Int(10);

  select;
    when pOAInfoDS.RpgOperation = QrnOperation_OPEN;
      pOAInfoDS.stateInfo = %Alloc( %Size(StateDS) );

      reset SPCADS;
      reset ErrDS;
      RetrieveUserSpaceAttributes( SPCADS
                                 : %Size(SPCADS)
                                 : 'SPCA0100'
                                 : QualDS
                                 : ErrDS );
      StateDS.UserSpaceSize = SPCADS.SpaceSize;
      QualDS.LibraryName = SPCADS.LibraryName;

      // If our user space isn't at least big enough to hold GENHDR0100 this probably
      //  wasn't populated with an IBM i List API
      if StateDS.UserSpaceSize < GENHDR0100_SIZE;
        // error
      endif;

      // Retrieve just the user area and generic header size first
      RetrieveUserSpace( QualDS
                       : 1
                       : GENHDR_SIZE
                       : GENHDR
                       : ErrDS );

      // Use the generic header size to determine if this is a *PGM or subproc list
      //  and load either GENHDR0100 or GENHDR0300 respectively
      select;
        when GENHDR.SizeOfHeader = GENHDR0100_SIZE - 64;
          RetrieveUserSpace( QualDS
                           : 1
                           : GENHDR0100_SIZE
                           : GENHDR0100
                           : ErrDS );

          StateDS.RecordCount = GENHDR0100.NumOfEntries;
          StateDS.RecordFormat = GENHDR0100.FormatName;
          StateDS.CurrentRecord = 1;
          StateDS.ListOffset = GENHDR0100.ListOffset;

        when GENHDR.SizeOfHeader = GENHDR0300_SIZE - 64;
          RetrieveUserSpace( QualDS
                           : 1
                           : GENHDR0300_SIZE
                           : GENHDR0300
                           : ErrDS );

          StateDS.RecordCount = GENHDR0300.NumOfEntries;
          StateDS.RecordFormat = GENHDR0300.FormatName;
          StateDS.CurrentRecord = 1;
          StateDS.ListOffset = GENHDR0300.ListOffset;

        other;
          // error
      endsl;

    when pOAInfoDS.RpgOperation = QrnOperation_CLOSE;
      // Unsure we need to do anything?
      dealloc(en) pOAInfoDS.stateInfo;

    when pOAInfoDS.RpgOperation = QrnOperation_POSITION_START;
      StateDS.CurrentRecord = 1;

    when pOAInfoDS.RpgOperation = QrnOperation_POSITION_END;
      StateDS.CurrentRecord = StateDS.RecordCount;

    when pOAInfoDS.RpgOperation = QrnOperation_CHAIN;
      if pOAInfoDS.keyedFile;
        // error, only set up for rrn
      endif;

      if pOAInfoDS.rrn < StateDS.RecordCount and pOAInfoDS.rrn > 0;
        StateDS.CurrentRecord = pOAInfoDS.rrn;

        Offset = StateDS.ListOffset + 1 + ((StateDS.CurrentRecord - 1) * pOAInfoDS.inputBufferLen);

        RetrieveUserSpace( QualDS
                         : Offset
                         : pOAInfoDS.inputBufferLen
                         : Buffer
                         : ErrDS );
        pOAInfoDS.inputDataLen = pOAInfoDS.inputBufferLen;

        pOAInfoDS.Found = *On;
        pOAInfoDS.Eof = *Off;
      else;
        pOAInfoDS.Found = *Off;
        // error, invalid rrn
      endif;

    when pOAInfoDS.RpgOperation = QrnOperation_READ;
      if StateDS.CurrentRecord <= StateDS.RecordCount;
        Offset = StateDS.ListOffset + 1 + ((StateDS.CurrentRecord - 1) * pOAInfoDS.inputBufferLen);

        RetrieveUserSpace( QualDS
                         : Offset
                         : pOAInfoDS.inputBufferLen
                         : Buffer
                         : ErrDS );
        pOAInfoDS.inputDataLen = pOAInfoDS.inputBufferLen;

        StateDS.CurrentRecord += 1;
      else;
        pOAInfoDS.Eof = *On;
      endif;

    other;
      pOAInfoDS.rpgStatus = 1299;
  endsl;

  return;

End-Proc;
