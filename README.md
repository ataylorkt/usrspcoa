# USRSPCOA
This repo contains an RPG Open Access handler to allow User Space (*USRSPC) objects which contain input from IBM List APIs such as QUSLOBJ to be easily accessed with native IO. 
 
RPG OA is **free** and part of the RPG compiler for IBM i 6.1+ as of Jan 2012 - however, your system may need [PTFs](https://www.ibm.com/developerworks/community/wikis/home?lang=en#/wiki/We13116a562db_467e_bcd4_882013aec57a/page/Open%20Access%20announcement) in order to compile or use this code. 
 
## Build
You can build this using [Relic](https://github.com/OSSILE/RelicPackageManager/): 

`RELICGET PLOC('https://bitbucket.org/ataylorkt/usrspcoa/downloads/ataylorkt-usrspcoa-master.zip') PDIR('ataylorkt-usrspcoa-master') PNAME(USRSPCOA)`

## Usage
An example program T_USRPRF is included. This program creates a user space in a specified library, populates it with a list of all *USRPRF objects in QSYS, and then prints them out via `DSPLY`. To call this program, you can do the following:

`CALL USRSPCOA/T_USRPRF Parm('TESTSPC   ' 'USRSPCOA  ')`

The RPG OA handler is subprocedure USRSPC_Handler in service program USRSPCOA. USRSPC_Handler uses the field definitions in a physical file to read from a list stored within a user space. 

For example, T_USRPRF is calling the [QUSLOBJ API](https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_71/apis/quslobj.htm) to populate the example user space, and is using the OBJL0100 format which looks like this:

| Offset Dec | Offset Hex | Type | Field |
|------------|------------|------|-------|
|0|0|CHAR(10)|Object name used|
|10|A|CHAR(10)|Object library name used|
|20|14|CHAR(10)|Object type used|

This can be represented with the following DDS:

~~~~
     A          R OBJL0100R
     A            OBJNAME       10A
     A            OBJLIB        10A
     A            OBJTYPE       10A   
~~~~

A physical file created with this DDS will (when defined with the Handler) communicate to USRSPC_Handler information about the record format and allow our T_USRPRF program to access the data in each list entry by accessing the OBJNAME, OBJLIB, and OBJTYPE fields from the physical file definition. The name of the file or the record format within do not matter. 

Reading the data from the entry is this easy:

~~~~
// Read through the user space and output all user profiles that were found
open OBJL0100_T;

read OBJL0100_T;
dow not %EOF(OBJL0100_T);
  dsply OBJNAME;
  read OBJL0100_T;
enddo;

close OBJL0100_T;
~~~~

Using this saves you as a developer from having to worry about retrieving the user space info via QUSRUSAT, reading it via QUSPTRUS or QUSRTVUS, worrying about calculating list entry offsets and not overstepping about the entry count within the user space, etc. Instead, you just populate your user space, and then work with it like you would a physical file.

## Notes
* DDS usage isn't required - this could be done just as easily with a table created via SQL. I think the concept is illustrated a bit easier with DDS, however, which is why I went that route.
* All of this code was written using full free format and thus has a minimum requirement of IBM i 7.1. However, if converted to fixed+free the code should work just fine on IBM i 6.1. If there is any interest in a fixed+free version, I will provide a branch which includes this.

## Contact
Aside from this repository, I can be reached via email at [ataylor@krengeltech.com](mailto:ataylor@krengeltech.com) and on Twitter [@adam_on_i](https://twitter.com/adam_on_i). 
